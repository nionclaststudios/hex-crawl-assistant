﻿public sealed class AppSystems : Feature
{

   public AppSystems( Contexts contexts, Terrains terrains )
   {
      // Input
      Add( new UISystems( contexts, terrains ) );
      // Update
      Add( new HexSystems( contexts ) );

      // Events
      Add( new EventSystems( contexts ) );

      // Cleanup
   }
}