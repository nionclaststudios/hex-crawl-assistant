﻿using Entitas;
using UnityEngine;

public class GameController : MonoBehaviour
{

   public Services services = Services.singleton;
   public Terrains TerrainsToUse;
   public GameObject TerrainToolUI;

   Systems _systems;

   void Awake()
   {
      var contexts = Contexts.sharedInstance;
      services.Initialize( contexts, this );
      _systems = new AppSystems( contexts, TerrainsToUse );
   }

   void Start()
   {
      _systems.Initialize();

      //TEMP
      TerrainToolUI.GetComponent<TerrainTool>().SetListener();
   }

   void Update()
   {
      _systems.Execute();
      _systems.Cleanup();
   }

   void OnDestroy()
   {
      _systems.TearDown();
   }
}