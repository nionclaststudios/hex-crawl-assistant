﻿using UnityEngine;
using Entitas;
using System;
using System.Collections.Generic;

public class TerrainTool : MonoBehaviour, ITerrainOptionsListener
{
   public ToolView view;
   public GameObject TerrainSelector;

	// Use this for initialization
	void Start ()
   {
      view = GetComponent<ToolView>();
	}

   public void SetListener()
   {
      Contexts.sharedInstance.uI.terrainOptionsEntity.AddTerrainOptionsListener( this );
   }

   private GameObject CreateToolOptionObject( TerrainType option )
   {
      var selectorObject = Instantiate( TerrainSelector );
      var selector = selectorObject.GetComponent<TerrainSelector>();
      selector.SetTerrain( option );
      return selectorObject;
   }

   public void OnTerrainOptions( UIEntity entity, List<TerrainType> TerrainOptions )
   {
      foreach( var option in TerrainOptions )
      {
         GameObject toolOption = CreateToolOptionObject(option);
         view.AddOption( toolOption );
      }
   }
}
