﻿using Entitas;
using Entitas.CodeGeneration.Attributes;
using System.Collections.Generic;

[UI]
[Unique]
[Event( true )]
public class TerrainOptionsComponent : IComponent
{
   public List<TerrainType> TerrainOptions;
}
