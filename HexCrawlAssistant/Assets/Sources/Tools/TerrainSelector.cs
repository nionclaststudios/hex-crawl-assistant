﻿using UnityEngine;
using UnityEngine.UI;

public class TerrainSelector : MonoBehaviour
{
   TerrainType _terrain;
   Button button;

   void Awake()
   {
      button = GetComponent<Button>();
   }

   public void SetTerrain( TerrainType terrain )
   {
      if( terrain == null )
         return;

      _terrain = terrain;
      var colors = button.colors;
      colors.normalColor = _terrain.BackupColor;
      button.colors = colors;


      var text = button.GetComponentInChildren<Text>();
      if( text != null )
      {
         text.text = terrain.name;
      }
   }

   public void SelectTerrain()
   {
      SelectedTerrainComponent selected = Contexts.sharedInstance.uI.selectedTerrain;
      selected.SelectedTerrainType = _terrain;
   }

}
