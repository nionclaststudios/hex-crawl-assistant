﻿using System.Collections.Generic;
using Entitas;

public class TerrainOptionPopulatorSystem : IInitializeSystem
{
   private UIContext _uiContext;
   private Terrains _terrains;

   public TerrainOptionPopulatorSystem( Contexts contexts, Terrains terrains )
   {
      _uiContext = contexts.uI;
      _terrains = terrains;
   }

   public void Initialize()
   {
      List<TerrainType> terrainOptions = new List<TerrainType>();

      foreach( var terrain in _terrains.TerrainTypes )
      {
         terrainOptions.Add( terrain );
      }

      UIEntity e = _uiContext.CreateEntity();
      e.AddTerrainOptions( terrainOptions );
      e.AddSelectedTerrain( terrainOptions[0] );
   }

}
