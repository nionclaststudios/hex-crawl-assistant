﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[UI]
[Unique]
public class SelectedTerrainComponent : IComponent
{
   public TerrainType SelectedTerrainType;
}
