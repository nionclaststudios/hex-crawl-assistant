﻿using UnityEngine;

[CreateAssetMenu]
public class TerrainType : ScriptableObject
{
   public TerrainType( string terrainPath, Color backupColor )
   {
      TerrainPath = terrainPath;
      BackupColor = backupColor;
   }

   public string TerrainPath;
   public Color BackupColor;
}
