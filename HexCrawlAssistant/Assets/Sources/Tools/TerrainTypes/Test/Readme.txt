﻿To add a new Terrain type:
1. right click add TerrainType from unity
2. Name it the name to show in the UI
3. Select a color (NOTE: Ensure alpha value is set)
4. Drag the newly created asset to the TestTerrains TerrainType list
Donezo