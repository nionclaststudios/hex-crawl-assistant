﻿using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu]
public class Terrains : ScriptableObject
{
   public List<TerrainType> TerrainTypes = new List<TerrainType>();
}
