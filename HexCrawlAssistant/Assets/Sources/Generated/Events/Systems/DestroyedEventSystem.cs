//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.EventSystemGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed class DestroyedEventSystem : Entitas.ReactiveSystem<HexEntity> {

    public DestroyedEventSystem(Contexts contexts) : base(contexts.hex) {
    }

    protected override Entitas.ICollector<HexEntity> GetTrigger(Entitas.IContext<HexEntity> context) {
        return Entitas.CollectorContextExtension.CreateCollector(
            context, Entitas.TriggerOnEventMatcherExtension.Added(HexMatcher.Destroyed)
        );
    }

    protected override bool Filter(HexEntity entity) {
        return entity.isDestroyed && entity.hasDestroyedListener;
    }

    protected override void Execute(System.Collections.Generic.List<HexEntity> entities) {
        foreach (var e in entities) {
            
            foreach (var listener in e.destroyedListener.value) {
                listener.OnDestroyed(e);
            }
        }
    }
}
