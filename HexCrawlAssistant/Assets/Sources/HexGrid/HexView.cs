﻿using System;
using Entitas;
using Entitas.Unity;
using UnityEngine;
using System.Collections.Generic;

public class HexView : MonoBehaviour, IView, IDestroyedListener, IHexPositionListener, ITerrainListener
{
   public GameObject BackgroundBorder;
   public GameObject Border;
   public GameObject HexBackground;
   public GameObject MouseOverHex;

   public List<GameObject> Highlights;

   public float Size = .5f;
   public virtual void Link( IEntity entity, IContext context )
   {
      gameObject.Link( entity, context );
      var e = (HexEntity)entity;
      e.AddHexPositionListener( this );
      e.AddDestroyedListener( this );
      e.AddTerrainListener( this );
      e.OnComponentRemoved += E_OnComponentRemoved;

      var pos = e.hexPosition;
      transform.localPosition = HexGridPositionToWorldPosition( pos.q, pos.r );
   }

   private void E_OnComponentRemoved( IEntity entity, int index, IComponent component )
   {
      if( component is TerrainComponent )
      {
         HexBackground.SetActive( false );
      }
   }

   public virtual void OnDestroyed( HexEntity entity )
   {
      destroy();
   }

   protected void destroy()
   {
      gameObject.Unlink();
      Destroy( gameObject );
   }

   public void OnHexPosition( HexEntity entity, int row, int column )
   {
      transform.localPosition = HexGridPositionToWorldPosition( row, column );
   }

   private Vector3 HexGridPositionToWorldPosition( int row, int column )
   {
      float x = (column*2 + row%2) * Size * Mathf.Sqrt( 3 )/2;
      float y = Size * 3 / 2 * row;
      return new Vector3( x, y );
   }

   public virtual void OnMouseEnter()
   {
      MouseOverHex.SetActive( true );
   }

   public virtual void OnMouseExit()
   {
      MouseOverHex.SetActive( false );
   }

   public virtual void OnMouseOver()
   {
      if( Input.GetButton( "LeftMouse" ) )
      {
         InputEntity ie = Contexts.sharedInstance.input.CreateEntity();
         ie.AddHexClick( (HexEntity)gameObject.GetEntityLink().entity );
      }
   }

   public void OnTerrain( HexEntity entity, TerrainType terrainType )
   {
      HexBackground.SetActive( true );
      var spriteRenderer = HexBackground.GetComponentInChildren<SpriteRenderer>();
      if( string.IsNullOrEmpty( terrainType.TerrainPath ) )
      {
         spriteRenderer.color = terrainType.BackupColor;
      }
   }
}