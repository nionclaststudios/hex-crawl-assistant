﻿using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

[Hex]
[Event( true )]
public class TerrainComponent : IComponent
{
   public TerrainType Terrain;
}
