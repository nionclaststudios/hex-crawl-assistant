﻿public sealed class HexSystems : Feature {

	public HexSystems( Contexts contexts )
   {
      Add( new HexGridSystem( contexts ) );
      Add( new HexViewCreationSystem( contexts ) );
      Add( new ApplyToolSystem( contexts ) );
   }
}
