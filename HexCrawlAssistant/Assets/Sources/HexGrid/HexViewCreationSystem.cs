﻿using System;
using System.Collections.Generic;
using Entitas;
using UnityEngine;

public class HexViewCreationSystem : ReactiveSystem<HexEntity>, IInitializeSystem
{
   readonly private IGroup<HexEntity> _hexElements;
   private HexGridComponent _hexGrid;
   HexContext _hexContext;

   public HexViewCreationSystem( Contexts contexts ) : base( contexts.hex )
   {
      _hexContext = contexts.hex;
      _hexElements = _hexContext.GetGroup( HexMatcher.AllOf( HexMatcher.HexGridElement, HexMatcher.HexPosition ) );
   }

   public void Initialize()
   {
      foreach( var hex in _hexElements )
      {
         CreateView( hex );
      }
   }

   private GameObject CreateView( HexEntity hex )
   {
      var hexObject = GameObject.Instantiate( Resources.Load("Hex") ) as GameObject;
      HexView view = hexObject.GetComponent<HexView>();
      view.Link( hex, _hexContext );

      return hexObject;
   }

   protected override void Execute( List<HexEntity> entities )
   {
      foreach( var entity in entities )
      {
         CreateView( entity );
      }
   }

   protected override bool Filter( HexEntity entity )
   {
      return true;
   }

   protected override ICollector<HexEntity> GetTrigger( IContext<HexEntity> context )
   {
      return context.CreateCollector( HexMatcher.HexGridElement.Added() );
   }
}
