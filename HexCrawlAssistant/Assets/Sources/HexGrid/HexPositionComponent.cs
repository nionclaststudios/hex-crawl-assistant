﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Hex]
[Event(true)]
public class HexPositionComponent : IComponent
{
   //Axial Coordinates
   public int q;
   public int r;
}
