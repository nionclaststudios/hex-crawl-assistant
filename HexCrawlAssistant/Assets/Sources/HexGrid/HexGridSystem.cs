﻿using Entitas;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HexGridSystem : IInitializeSystem
{
   readonly private IGroup<HexEntity> _hexElements;
   private HexGridComponent _hexGrid;
   HexContext _hexContext;

   public HexGridSystem( Contexts contexts ) //: base( contexts.hex )
   {
      _hexContext = contexts.hex;
      _hexElements = _hexContext.GetGroup( HexMatcher.AllOf( HexMatcher.HexGridElement, HexMatcher.HexPosition ) );
   }

   public void Initialize()
   {
      //create grid
      _hexGrid = _hexContext.SetHexGrid( 5, 5 ).hexGrid;

      //Fill grid
      for( int r = 0; r < _hexGrid.rows; r++ )
      {
         for( int q = 0; q < _hexGrid.columns; q++ )
         {
            HexEntity e = _hexContext.CreateEntity();
            e.AddHexPosition( q, r );
            e.isHexGridElement = true;
         }
      }
   }

   //protected override void Execute( List<HexEntity> entities )
   //{
   //   throw new NotImplementedException();
   //}

   //protected override bool Filter( HexEntity entity )
   //{
   //   throw new NotImplementedException();
   //}

   //protected override ICollector<HexEntity> GetTrigger( IContext<HexEntity> context )
   //{
   //   throw new NotImplementedException();
   //}
}
