﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Unique]
[Hex]
[Event(true)]
public sealed class HexGridComponent : IComponent
{
   [EntityIndex]
   public int rows;
   [EntityIndex]
   public int columns;
}
