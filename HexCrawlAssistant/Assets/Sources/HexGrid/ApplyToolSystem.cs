﻿using System;
using System.Collections.Generic;
using Entitas;

public class ApplyToolSystem : ReactiveSystem<InputEntity>
{
   InputContext _inputContext;
   UIContext _uiContext;
   readonly private IGroup<InputEntity> _hexClickElements;

   public ApplyToolSystem( Contexts contexts ) : base( contexts.input )
   {
      _inputContext = contexts.input;
      _uiContext = contexts.uI;
      _hexClickElements = _inputContext.GetGroup( InputMatcher.AllOf( InputMatcher.HexClick ) );
   }

   protected override void Execute( List<InputEntity> entities )
   {
      foreach( var entity in entities )
      {
         var clickedHex = entity.hexClick.clickedHex;

         TerrainType currentSelectedTerrain = _uiContext.selectedTerrain.SelectedTerrainType;
         if( !clickedHex.hasTerrain )
         {
            clickedHex.AddTerrain( currentSelectedTerrain );
         }
         else
         {
            clickedHex.ReplaceTerrain( currentSelectedTerrain );
         }
         entity.Destroy();
      }
   }

   protected override bool Filter( InputEntity entity )
   {
      return true;
   }

   protected override ICollector<InputEntity> GetTrigger( IContext<InputEntity> context )
   {
      return context.CreateCollector( InputMatcher.HexClick.Added() );
   }
}
