﻿public sealed class UISystems : Feature
{
   public UISystems( Contexts contexts, Terrains terrains )
   {
      Add( new TerrainOptionPopulatorSystem( contexts, terrains ) );
   }
}
