﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToolView : MonoBehaviour
{
   public Text Title;
   public GameObject ToolContentHost;

   private List<GameObject> _toolOptionObjects = new List<GameObject>();

   public void AddOption( GameObject option )
   {
      _toolOptionObjects.Add( option );
      option.transform.SetParent( ToolContentHost.transform );
   }

   public void RemoveOption( GameObject option )
   {
      if( _toolOptionObjects.Contains( option ) )
      {
         option.transform.SetParent( null );
         _toolOptionObjects.Remove( option );
      }
   }
}
